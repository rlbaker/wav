# wav

## Overview

This goal of this project is to build a functional WAVE audio encoder and decoder using Rust.
For now, the focus will be on PCM ([Pulse-Code Modulation](https://en.wikipedia.org/wiki/Pulse-code_modulation)) encoded data.

At the moment, only 16-bit and 24-bit signed integer encoded audio is supported.


## Data Layout

The WAVE file format is a subset of the RIFF (Resource Interchange File Format)
specification that is used for storing audio data. Audio data is uncompressed
and unless otherwise stated, all integers are [little-endian](https://en.wikipedia.org/wiki/Endianness).
Due to the 32-bit file size field, WAVE files cannot exceed 4GB in size.

The following chart shows the data layout for a basic PCM WAV file.
There are various extensions to the RIFF format that WAV can take
advantage of to handle things like tag metadata (artist, album, etc.).
These will appear after the Data chunk and begin with a 4-byte chunk id and
4-byte chunk size. This project currently ignores these extra chunks for simplicity.

![Data Layout](wav_layout.png)

| Offset (Bytes) | Length (Bytes) |    Section   | Contents |
| -------------- | -------------- | ------------ | -------- |
|              0 |             12 | Header       | RIFF header containing RIFF tag, file size and format tag |
|             12 |             24 | Format Chunk | Audio format metadata (channels, sample rate, bit-depth) |
|             36 |            END | Data Chunk   | Size metadata and raw sound data |


### Header

The header contains basic information tagging the file as a RIFF WAV file.
It's worth mentioning that the `chunk_size` field contains the *remaining* data
in the file. To calculate the total file size, add **8** to this value.

| Offset (Bytes) | Length (Bytes) |   Field Name  | Contents |
| -------------- | -------------- | ------------- | -------- |
|              0 |              4 | `chunk_id`    | ASCII: "**RIFF**", HEX: `0x52 0x49 0x46 0x46` |
|              4 |              4 | `chunk_size`  | 32-bit file size in bytes minus **8** bytes for `chunk_id` and `chunk_size` |
|              8 |              4 | `file_format` | ASCII: "**WAVE**", HEX: `0x57 0x41 0x56 0x45` |


### Format Chunk

The format chunk is used to encode information describing the audio stream,
such as number of channels, audio sample rate, and bit depth.

| Offset (bytes) | Length (bytes) |   Field Name   | Contents |
| -------------- | -------------- | -------------- | -------- |
|             12 |              4 | `chunk_id`     | ASCII: "**fmt&nbsp;**", HEX: `0x66 0x6D 0x74 0x20` (Note the space in the string) |
|             16 |              4 | `chunk_size`   | Size (in bytes) of the remaning data in this chunk. This will always be **16** for PCM. |
|             20 |              2 | `audio_format`   | This will always be a value of **1** for PCM. |
|             22 |              2 | `channels`     | Number of audio channels. **1** = Mono, **2** = Stereo, ... |
|             24 |              4 | `sample_rate`  | Audio sample rate in hertz. Common values: **44100**, **48000**, **96000** |
|             28 |              4 | `byte_rate`    | Bytes per second to read from all channels. $`sample\_rate * frame\_size`$ |
|             32 |              2 | `frame_size`   | Number of bytes to read 1 sample from all channels. $`channels * (bit\_depth/8)`$ |
|             34 |              2 | `bit_depth`    | Number of bits in each sample. Common values: **8**, **16**, **24**, **32** |

### Data Chunk

The data chunk stores the raw, uncompressed audio data.

| Offset (bytes) | Length (bytes) |   Field Name   | Contents |
| -------------- | -------------- | -------------- | -------- |
|             36 |              4 | `chunk_id`     | ASCII: "**data**", HEX: `0x64 0x61 0x74 0x61` |
|             40 |              4 | `chunk_size`   | Size in bytes of raw audio data following this field. |
|             44 |   `chunk_size` | `data`         | Raw audio data |

The datatype used to represent a sample varies depending on the
bit depth. Rust datatypes are also provided in this table to match
what will be seen in the codebase. Note that 24-bit audio is
represented by a 32-bit signed integer because Rust does not provide
a 24-bit integer datatype out of the box. When decoding 24-bit audio data,
it is important to remember that even though it will be represented by a
32-bit integer, one should only read 24-bits (3 bytes) from the data stream.

| Bit Depth |     Representation     | Rust Type |
| --------- | ---------------------- | --------- |
|        16 | 16-bit signed integer  |     `i16` |
|        24 | 24-bit signed integer  |     `i32` |

