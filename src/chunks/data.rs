use std::io::{Error, ErrorKind, Read, Result};

use byteorder::{BigEndian, LittleEndian, ReadBytesExt};

use chunks::Format;

#[derive(Debug)]
pub struct Data {
    chunk_size: u32,
    pub samples: Vec<i32>,
}

impl Data {
    pub fn read<R: Read>(reader: &mut R, format: &Format) -> Result<Self> {
        // 0x64_61_74_61 == "data"
        let riff_tag = reader.read_u32::<BigEndian>()?;
        if riff_tag != 0x64_61_74_61 {
            return Err(Error::new(ErrorKind::InvalidData,
                                  format!("Invalid data tag: {:?}", riff_tag)));
        }

        let chunk_size = reader.read_u32::<LittleEndian>()?;

        let byte_depth = format.bit_depth as usize / 8;
        let reads_needed = chunk_size as usize / byte_depth;
        let mut samples = Vec::with_capacity(reads_needed);

        for _ in 0..reads_needed {
            let sample = reader.read_int::<LittleEndian>(byte_depth)?;
            samples.push(sample as i32)
        }

        Ok(Data {
               chunk_size: chunk_size,
               samples: samples,
           })
    }
}
