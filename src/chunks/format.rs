use std::io::{Error, ErrorKind, Read, Result};

use byteorder::{BigEndian, LittleEndian, ReadBytesExt};

#[derive(Debug)]
pub struct Format {
    chunk_size: u32,
    audio_format: u16,
    pub channels: u16,
    pub sample_rate: u32,
    pub byte_rate: u32,
    pub frame_size: u16,
    pub bit_depth: u16,
}

impl Format {
    pub fn read<R: Read>(reader: &mut R) -> Result<Format> {
        // 0x66_6d_74_20 == "fmt "
        let riff_tag = reader.read_u32::<BigEndian>()?;
        if riff_tag != 0x66_6d_74_20 {
            return Err(Error::new(ErrorKind::InvalidData,
                                  format!("Invalid format tag: {:?}", riff_tag)));
        }

        // chunk_size should be exactly 16 for PCM files
        let chunk_size = reader.read_u32::<LittleEndian>()?;
        if chunk_size != 16 {
            return Err(Error::new(ErrorKind::InvalidData,
                                  format!("Invalid format chunk size: {:?}", chunk_size)));
        }

        // audio_format should be 1 for PCM files
        let audio_format = reader.read_u16::<LittleEndian>()?;
        if audio_format != 1 {
            return Err(Error::new(ErrorKind::InvalidData,
                                  format!("Unsupported audio format: {:?}", audio_format)));
        }

        let channels = reader.read_u16::<LittleEndian>()?;
        let sample_rate = reader.read_u32::<LittleEndian>()?;
        let byte_rate = reader.read_u32::<LittleEndian>()?;
        let frame_size = reader.read_u16::<LittleEndian>()?;
        let bit_depth = reader.read_u16::<LittleEndian>()?;

        Ok(Format {
               chunk_size: chunk_size,
               audio_format: audio_format,
               channels: channels,
               sample_rate: sample_rate,
               byte_rate: byte_rate,
               frame_size: frame_size,
               bit_depth: bit_depth,
           })
    }
}

#[cfg(test)]
mod test {
    use std::io::{Cursor, Write};
    use byteorder::{LittleEndian, WriteBytesExt};

    use super::Format;

    fn build_format_chunk(audio_format: u16,
                          channels: u16,
                          sample_rate: u32,
                          bit_depth: u16)
                          -> Cursor<Vec<u8>> {
        // Use in memory buffer rather than reading from a file
        let mut buf = Cursor::new(vec![]);

        buf.write_all(b"fmt ").unwrap(); // chunk_id
        buf.write_u32::<LittleEndian>(16).unwrap(); // chunk_size
        buf.write_u16::<LittleEndian>(audio_format).unwrap();
        buf.write_u16::<LittleEndian>(channels).unwrap();
        buf.write_u32::<LittleEndian>(sample_rate).unwrap();

        // These properties can be calcuated from the 3 function parameters
        let frame_size = channels * bit_depth;
        let byte_rate = sample_rate * (frame_size as u32);
        buf.write_u32::<LittleEndian>(byte_rate).unwrap();
        buf.write_u16::<LittleEndian>(frame_size).unwrap();

        buf.write_u16::<LittleEndian>(bit_depth).unwrap();

        // Reset cursor position
        buf.set_position(0);
        buf
    }


    #[test]
    fn format_valid() {
        // PCM, 2 channel, 44.1khz, 16-bit
        let mut buf = build_format_chunk(1, 2, 44100, 16);
        let res = Format::read(&mut buf);

        let f = res.unwrap();
        assert_eq!(f.channels, 2);
        assert_eq!(f.sample_rate, 44100);
        assert_eq!(f.bit_depth, 16);

        // PCM, 6 channel, 96khz, 24-bit
        buf = build_format_chunk(1, 6, 96000, 24);
        let res = Format::read(&mut buf);

        let f = res.unwrap();
        assert_eq!(f.channels, 6);
        assert_eq!(f.sample_rate, 96000);
        assert_eq!(f.bit_depth, 24);
    }

    #[test]
    fn format_invalid_audio_format() {
        let mut buf = build_format_chunk(0, 2, 44100, 16);
        let res = Format::read(&mut buf);
        assert!(res.is_err());

        let mut buf = build_format_chunk(2, 2, 44100, 16);
        let res = Format::read(&mut buf);
        assert!(res.is_err());
    }
}
