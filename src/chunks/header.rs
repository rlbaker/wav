use std::io::{Error, ErrorKind, Read, Result};

use byteorder::{BigEndian, LittleEndian, ReadBytesExt};

#[derive(Debug)]
pub struct Header {
    chunk_size: u32,
}

impl Header {
    pub fn read<R: Read>(reader: &mut R) -> Result<Header> {
        // 0x52_49_46_46 == "RIFF"
        let riff_tag = reader.read_u32::<BigEndian>()?;
        if riff_tag != 0x52_49_46_46 {
            return Err(Error::new(ErrorKind::InvalidData,
                                  format!("Invalid header tag: {:?}", riff_tag)));
        }

        // chunk_size is the amount of remaining data in the file and must be at least 4 bytes to
        // account for the trailing file_format field, making the minimum valid file size 12 bytes
        let chunk_size = reader.read_u32::<LittleEndian>()?;
        if chunk_size < 4 {
            return Err(Error::new(ErrorKind::InvalidData,
                                  format!("Invalid header chunk size: {:?}", chunk_size)));
        }

        // 0x57_41_56_45 == "WAVE"
        let file_format = reader.read_u32::<BigEndian>()?;
        if file_format != 0x57_41_56_45 {
            return Err(Error::new(ErrorKind::InvalidData,
                                  format!("Invalid file format: {:?}", file_format)));
        }

        Ok(Header { chunk_size: chunk_size })
    }
}

#[cfg(test)]
mod test {
    use std::io::{Cursor, Write};
    use byteorder::{LittleEndian, WriteBytesExt};

    use super::Header;

    fn build_valid_header(size: u32) -> Cursor<Vec<u8>> {
        // Use in memory buffer rather than reading from a file
        let mut buf = Cursor::new(vec![]);

        buf.write_all(b"RIFF").is_ok(); // chunk_id
        buf.write_u32::<LittleEndian>(size).is_ok();
        buf.write_all(b"WAVE").is_ok(); // file_format

        // Reset cursor position
        buf.set_position(0);
        buf
    }

    #[test]
    fn header_valid() {
        let mut buf = build_valid_header(31680);

        let res = Header::read(&mut buf);
        assert!(res.is_ok());

        let h = res.unwrap();
        assert_eq!(h.chunk_size, 31680)
    }

    #[test]
    fn header_minimum_size() {
        let mut buf = build_valid_header(4);

        let res = Header::read(&mut buf);
        assert!(res.is_ok());

        let h = res.unwrap();
        assert_eq!(h.chunk_size, 4)
    }

    fn build_invalid_header(id: &[u8], size: u32, file_format: &[u8]) -> Cursor<Vec<u8>> {
        // Use in memory buffer rather than reading from a file
        let mut buf = Cursor::new(vec![]);

        buf.write_all(id).is_ok();
        buf.write_u32::<LittleEndian>(size).is_ok();
        buf.write_all(file_format).is_ok();

        // Reset cursor position
        buf.set_position(0);

        buf
    }

    #[test]
    fn header_invalid_chunk_id() {
        let mut buf = build_invalid_header(b"riff", 4, b"WAVE");
        let res = Header::read(&mut buf);
        assert!(res.is_err());
    }

    #[test]
    fn header_invalid_chunk_size() {
        let mut buf = build_invalid_header(b"RIFF", 0, b"WAVE");
        let res = Header::read(&mut buf);
        assert!(res.is_err());
    }

    #[test]
    fn header_invalid_file_format() {
        let mut buf = build_invalid_header(b"RIFF", 4, b"wave");
        let res = Header::read(&mut buf);
        assert!(res.is_err());
    }
}
