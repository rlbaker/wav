extern crate byteorder;

mod chunks;
mod wav;

pub use wav::Wav;
