use std::io::{BufReader, Read, Result};
use std::fs::File;
use std::path::Path;

use chunks::{Data, Format, Header};

pub struct Wav {
    pub header: Header,
    pub format: Format,
    pub data: Data,
}

impl Wav {
    pub fn read<R: Read>(reader: &mut R) -> Result<Self> {
        let header = Header::read(reader)?;
        let format = Format::read(reader)?;
        let data = Data::read(reader, &format)?;

        Ok(Wav {
               header: header,
               format: format,
               data: data,
           })
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Self> {
        let f = File::open(path)?;
        let mut reader = BufReader::new(f);

        Self::read(&mut reader)
    }
}
